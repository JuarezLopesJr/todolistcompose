package com.example.todolistcompose.repository

import com.example.todolistcompose.model.TodoDao
import com.example.todolistcompose.model.TodoTask
import dagger.hilt.android.scopes.ViewModelScoped
import javax.inject.Inject

@ViewModelScoped
class TodoRepository @Inject constructor(private val todoDao: TodoDao) {
    /* when returning a hole list, put the returned value inside a variable */
    val getAllTasksRepo = todoDao.getAllTasksDao()

    val sortByLowPriorityRepo = todoDao.sortByLowPriorityDao()

    val sortByMediumPriorityRepo = todoDao.sortByMediumPriorityDao()

    val sortByHighPriorityRepo = todoDao.sortByHighPriorityDao()

    fun getSelectedTaskRepo(taskId: Int) = todoDao.getSelectedTaskDao(taskId)

    suspend fun addTaskRepo(task: TodoTask) = todoDao.addTaskDao(task)

    suspend fun updateTaskRepo(task: TodoTask) = todoDao.updateTaskDao(task)

    suspend fun deleteTaskRepo(task: TodoTask) = todoDao.deleteTaskDao(task)

    suspend fun deleteAllTaskRepo() = todoDao.deleteAllTasksDao()

    fun searchDatabaseRepo(searchQuery: String) =
        todoDao.searchDatabaseDao(searchQuery)
}