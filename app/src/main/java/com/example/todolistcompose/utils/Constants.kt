package com.example.todolistcompose.utils

object Constants {
    const val DATABASE_TABLE = "todo_table"
    const val DATABASE_NAME = "todo_database"
    const val ROUTE_SPLASH = "splash"
    const val ROUTE_LIST = "list"
    const val ROUTE_TASK = "task"
    const val LIST_SCREEN = "list/{action}"
    const val TASK_SCREEN = "task/{taskId}"
    const val LIST_ARGUMENT_KEY = "action"
    const val TASK_ARGUMENT_KEY = "taskId"
    const val MAX_TITLE_LENGTH = 20
    const val DELETE_LABEL = "DELETE"
    const val UNDO_LABEL = "UNDO"
    const val CONFIRM_LABEL = "OK"
    const val DELETED_ALL_TASKS_MESSAGE = "All tasks removed"
    const val PREFERENCE_NAME = "todo_preferences"
    const val PREFERENCE_KEY = "sort_state"
}