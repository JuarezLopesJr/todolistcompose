package com.example.todolistcompose.model

import androidx.compose.ui.graphics.Color
import com.example.todolistcompose.ui.theme.HighPriorityColor
import com.example.todolistcompose.ui.theme.LowPriorityColor
import com.example.todolistcompose.ui.theme.MediumPriorityColor
import com.example.todolistcompose.ui.theme.NonePriorityColor

enum class Priority(val color: Color) {
    HIGH(HighPriorityColor),
    MEDIUM(MediumPriorityColor),
    LOW(LowPriorityColor),
    NONE(NonePriorityColor)
}