package com.example.todolistcompose.view

import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.size
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.todolistcompose.R
import com.example.todolistcompose.ui.theme.SPLASH_SCREEN_ICON_SIZE
import com.example.todolistcompose.ui.theme.TodoListComposeTheme
import com.example.todolistcompose.ui.theme.splashScreenBackground
import kotlinx.coroutines.delay

@Composable
fun SplashScreenCompose(navigateToListScreen: () -> Unit) {

    var startAnimation by remember {
        mutableStateOf(false)
    }

    val offSetState by animateDpAsState(
        targetValue = if (startAnimation) 0.dp else SPLASH_SCREEN_ICON_SIZE,
        animationSpec = tween(durationMillis = 1000)
    )

    val alphaState by animateFloatAsState(
        targetValue = if (startAnimation) 1f else 0f,
        animationSpec = tween(durationMillis = 1000)
    )

    LaunchedEffect(key1 = true) {
        startAnimation = true
        delay(3000)
        navigateToListScreen()
    }

    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(color = MaterialTheme.colors.splashScreenBackground),
        contentAlignment = Alignment.Center
    ) {
        Image(
            modifier = Modifier
                .size(SPLASH_SCREEN_ICON_SIZE)
                .offset(y = offSetState)
                .alpha(alphaState),
            painter = painterResource(R.drawable.ic_launcher_foreground),
            contentDescription = stringResource(R.string.splash_screen_icon_description)
        )
    }
}

@Preview
@Composable
fun SplashScreenPreview() {
    TodoListComposeTheme(darkTheme = true) {
        SplashScreenCompose(navigateToListScreen = {})
    }
}

/* setting the logo accordingly with the theme
@Composable
fun GetLogo() =
    if (isSystemInDarkTheme()) R.drawable.ic_launcher_foreground else R.drawable.ic_launcher_background*/
