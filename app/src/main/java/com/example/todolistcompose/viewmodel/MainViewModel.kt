package com.example.todolistcompose.viewmodel

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.todolistcompose.model.Priority
import com.example.todolistcompose.model.TodoTask
import com.example.todolistcompose.repository.DataStoreRepository
import com.example.todolistcompose.repository.TodoRepository
import com.example.todolistcompose.utils.ActionsDB
import com.example.todolistcompose.utils.Constants.MAX_TITLE_LENGTH
import com.example.todolistcompose.utils.RequestState
import com.example.todolistcompose.utils.SearchAppbarState
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch

@HiltViewModel
class MainViewModel @Inject constructor(
    private val repo: TodoRepository,
    private val dataStoreRepo: DataStoreRepository
) : ViewModel() {

    private val id = mutableStateOf(0)
    val title = mutableStateOf("")
    val description = mutableStateOf("")
    val priority = mutableStateOf(Priority.LOW)
    val action = mutableStateOf(ActionsDB.NO_ACTION)

    val searchAppbarState =
        mutableStateOf(SearchAppbarState.CLOSED)

    val searchTextState = mutableStateOf("")

    private val _allTasks =
        MutableStateFlow<RequestState<List<TodoTask>>>(RequestState.Idle)

    val allTasks: StateFlow<RequestState<List<TodoTask>>>
        get() = _allTasks

    private val _selectedTask = MutableStateFlow<TodoTask?>(null)

    val selectedTask: StateFlow<TodoTask?>
        get() = _selectedTask

    private val _searchedTasks =
        MutableStateFlow<RequestState<List<TodoTask>>>(RequestState.Idle)

    val searchedTasks: StateFlow<RequestState<List<TodoTask>>>
        get() = _searchedTasks

    private val _sortState =
        MutableStateFlow<RequestState<Priority>>(RequestState.Idle)

    val sortState: StateFlow<RequestState<Priority>>
        get() = _sortState

    /* convert this flow (cold flow) in to state flow (hot flow) */
    val lowPriorityTasks = repo.sortByLowPriorityRepo.stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(),
        initialValue = emptyList()
    )

    val highPriorityTasks = repo.sortByHighPriorityRepo.stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(),
        initialValue = emptyList()
    )

    private fun addTask() = viewModelScope.launch(Dispatchers.IO) {
        val todoTask = TodoTask(
            title = title.value,
            description = description.value,
            priority = priority.value
        )
        repo.addTaskRepo(todoTask)
        searchAppbarState.value = SearchAppbarState.CLOSED
    }

    private fun updateTask() = viewModelScope.launch(Dispatchers.IO) {
        val todoTask = TodoTask(
            id = id.value,
            title = title.value,
            description = description.value,
            priority = priority.value
        )
        repo.updateTaskRepo(todoTask)
    }

    private fun deleteTask() = viewModelScope.launch(Dispatchers.IO) {
        val todoTask = TodoTask(
            id = id.value,
            title = title.value,
            description = description.value,
            priority = priority.value
        )
        repo.deleteTaskRepo(todoTask)
    }

    private fun deleteAllTasks() = viewModelScope.launch(Dispatchers.IO) {
        repo.deleteAllTaskRepo()
    }

    fun handleDbActions(actionsDB: ActionsDB) {
        when (actionsDB) {
            ActionsDB.ADD -> {
                addTask()
            }
            ActionsDB.UPDATE -> {
                updateTask()
            }
            ActionsDB.DELETE -> {
                deleteTask()
            }
            ActionsDB.DELETE_ALL -> {
                deleteAllTasks()
            }
            ActionsDB.UNDO -> {
                /* re-inserting the deleted task */
                addTask()
            }
            else -> {
                ActionsDB.NO_ACTION
            }
        }
        action.value = ActionsDB.NO_ACTION
    }

    fun getAllTasks() = viewModelScope.launch(Dispatchers.IO) {
        _allTasks.value = RequestState.Loading
        try {
            repo.getAllTasksRepo.collect {
                _allTasks.value = RequestState.Success(it)
            }
        } catch (e: Exception) {
            _allTasks.value = RequestState.Error(e)
        }
    }

    fun searchDb(searchQuery: String) {
        _searchedTasks.value = RequestState.Loading
        try {
            viewModelScope.launch(Dispatchers.IO) {
                /* % is used to search char in SQL */
                repo.searchDatabaseRepo(searchQuery = "%$searchQuery%")
                    .collect {
                        _searchedTasks.value = RequestState.Success(it)
                    }
            }
        } catch (e: Exception) {
            _searchedTasks.value = RequestState.Error(e)
        }
        searchAppbarState.value = SearchAppbarState.TRIGGERED
    }

    fun getSelectedTask(taskId: Int) = viewModelScope.launch(Dispatchers.IO) {
        repo.getSelectedTaskRepo(taskId).collect {
            _selectedTask.value = it
        }
    }

    /* used to pre populate the fields when clicking in a task */
    fun updateTaskFields(selectedTask: TodoTask?) {
        if (selectedTask != null) {
            id.value = selectedTask.id
            title.value = selectedTask.title
            description.value = selectedTask.description
            priority.value = selectedTask.priority
        } else {
            id.value = 0
            title.value = ""
            description.value = ""
            priority.value = Priority.LOW
        }
    }

    fun updateTitle(newTitle: String) {
        /* character count */
        if (newTitle.length <= MAX_TITLE_LENGTH) {
            title.value = newTitle
        }
    }

    fun validateFields() = title.value.isNotEmpty() && description.value.isNotEmpty()

    fun persistSortedState(priority: Priority) =
        viewModelScope.launch(Dispatchers.IO) {
            dataStoreRepo.persistSortedState(priority = priority)
        }

    fun readSortState() {
        _sortState.value = RequestState.Loading
        try {
            viewModelScope.launch(Dispatchers.IO) {
                dataStoreRepo.readSortState
                    /* converting the String type in Priority to pass to .collect */
                    .map {
                        Priority.valueOf(it)
                    }
                    .collect {
                        _sortState.value = RequestState.Success(it)
                    }
            }
        } catch (e: Exception) {
            _sortState.value = RequestState.Error(e)
        }
    }
}