package com.example.todolistcompose.navigation

import androidx.navigation.NavHostController
import com.example.todolistcompose.utils.ActionsDB
import com.example.todolistcompose.utils.Constants.LIST_SCREEN
import com.example.todolistcompose.utils.Constants.ROUTE_LIST
import com.example.todolistcompose.utils.Constants.ROUTE_SPLASH
import com.example.todolistcompose.utils.Constants.ROUTE_TASK

class Routes(navController: NavHostController) {

    val navFromSplash: () -> Unit = {
        navController.navigate(route = "$ROUTE_LIST/${ActionsDB.NO_ACTION}") {
            popUpTo(ROUTE_SPLASH) { inclusive = true }
        }
    }

    val navFromList: (ActionsDB) -> Unit = { actionsDB ->
        navController.navigate(route = "$ROUTE_LIST/${actionsDB.name}") {
            popUpTo(LIST_SCREEN) { inclusive = true }
        }
    }

    val navFromTask: (Int) -> Unit = { taskId ->
        navController.navigate(route = "$ROUTE_TASK/$taskId")
    }
}