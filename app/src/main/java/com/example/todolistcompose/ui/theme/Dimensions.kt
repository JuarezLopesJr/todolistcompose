package com.example.todolistcompose.ui.theme

import androidx.compose.ui.unit.dp

val SMALL_PADDING = 6.dp
val MEDIUM_PADDING = 8.dp
val LARGE_PADDING = 12.dp
val LARGEST_PADDING = 24.dp
val SPLASH_SCREEN_ICON_SIZE = 100.dp

val PRIORITY_INDICATOR_SIZE = 16.dp
val TOP_APPBAR_HEIGHT = 56.dp
val TASK_ITEM_ELEVATION = 2.dp
val PRIORITY_DROPDOWN_HEIGHT = 60.dp
val PRIORITY_DROPDOWN_BORDER = 1.dp