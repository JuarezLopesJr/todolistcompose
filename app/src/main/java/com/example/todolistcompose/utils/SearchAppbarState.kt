package com.example.todolistcompose.utils

enum class SearchAppbarState {
    OPENED,
    CLOSED,
    TRIGGERED
}