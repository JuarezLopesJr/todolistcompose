package com.example.todolistcompose.navigation

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.example.todolistcompose.utils.Constants.LIST_ARGUMENT_KEY
import com.example.todolistcompose.utils.Constants.LIST_SCREEN
import com.example.todolistcompose.utils.Constants.ROUTE_SPLASH
import com.example.todolistcompose.utils.Constants.TASK_ARGUMENT_KEY
import com.example.todolistcompose.utils.Constants.TASK_SCREEN
import com.example.todolistcompose.utils.toAction
import com.example.todolistcompose.view.ListScreen
import com.example.todolistcompose.view.SplashScreenCompose
import com.example.todolistcompose.view.TaskScreen
import com.example.todolistcompose.viewmodel.MainViewModel

@ExperimentalAnimationApi
@ExperimentalMaterialApi
@Composable
fun NavigationCompose(
    navController: NavHostController,
    mainViewModel: MainViewModel = hiltViewModel()
) {
    val routes = remember(navController) {
        Routes(navController = navController)
    }

    NavHost(navController = navController, startDestination = ROUTE_SPLASH) {

        composable(route = ROUTE_SPLASH) {
            SplashScreenCompose(navigateToListScreen = routes.navFromSplash)
        }

        composable(
            route = LIST_SCREEN,
            arguments = listOf(navArgument(LIST_ARGUMENT_KEY) {
                type = NavType.StringType
            })
        ) {
            val action = it.arguments?.getString(LIST_ARGUMENT_KEY).toAction()

            LaunchedEffect(key1 = action) {
                mainViewModel.action.value = action
            }

            ListScreen(navigateToTaskScreen = routes.navFromTask, mainViewModel = mainViewModel)
        }

        composable(
            route = TASK_SCREEN,
            arguments = listOf(navArgument(TASK_ARGUMENT_KEY) {
                type = NavType.IntType
            })
        ) {
            val taskId = it.arguments!!.getInt(TASK_ARGUMENT_KEY)

            LaunchedEffect(key1 = taskId) { mainViewModel.getSelectedTask(taskId = taskId) }

            val selectedTask by mainViewModel.selectedTask.collectAsState()

            /* fix to recompose only when new selectedTask is collected */
            LaunchedEffect(key1 = selectedTask) {
                /* code to resolve UNDO deleted tasks */
                if (selectedTask != null || taskId == -1) {
                    mainViewModel.updateTaskFields(selectedTask)
                }
            }

            TaskScreen(
                mainViewModel = mainViewModel,
                selectedTask = selectedTask,
                navigateToListScreen = routes.navFromList
            )
        }
    }
}