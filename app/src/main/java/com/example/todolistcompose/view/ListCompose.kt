package com.example.todolistcompose.view

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.animation.expandVertically
import androidx.compose.animation.shrinkVertically
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.AppBarDefaults
import androidx.compose.material.ContentAlpha
import androidx.compose.material.DismissDirection
import androidx.compose.material.DismissValue
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.FloatingActionButton
import androidx.compose.material.FractionalThreshold
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.ScaffoldState
import androidx.compose.material.SnackbarDuration
import androidx.compose.material.SnackbarResult
import androidx.compose.material.Surface
import androidx.compose.material.SwipeToDismiss
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.rememberDismissState
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextOverflow
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.todolistcompose.R
import com.example.todolistcompose.model.Priority
import com.example.todolistcompose.model.TodoTask
import com.example.todolistcompose.ui.theme.HighPriorityColor
import com.example.todolistcompose.ui.theme.LARGEST_PADDING
import com.example.todolistcompose.ui.theme.LARGE_PADDING
import com.example.todolistcompose.ui.theme.PRIORITY_INDICATOR_SIZE
import com.example.todolistcompose.ui.theme.TASK_ITEM_ELEVATION
import com.example.todolistcompose.ui.theme.TOP_APPBAR_HEIGHT
import com.example.todolistcompose.ui.theme.Typography
import com.example.todolistcompose.ui.theme.fabBackgroundColor
import com.example.todolistcompose.ui.theme.taskItemBackgroundColor
import com.example.todolistcompose.ui.theme.taskItemTextColor
import com.example.todolistcompose.ui.theme.topAppbarBackground
import com.example.todolistcompose.ui.theme.topAppbarContentColor
import com.example.todolistcompose.utils.ActionsDB
import com.example.todolistcompose.utils.Constants.CONFIRM_LABEL
import com.example.todolistcompose.utils.Constants.DELETED_ALL_TASKS_MESSAGE
import com.example.todolistcompose.utils.Constants.DELETE_LABEL
import com.example.todolistcompose.utils.Constants.UNDO_LABEL
import com.example.todolistcompose.utils.RequestState
import com.example.todolistcompose.utils.SearchAppbarState
import com.example.todolistcompose.utils.TrailingIconState
import com.example.todolistcompose.viewmodel.MainViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@ExperimentalAnimationApi
@ExperimentalMaterialApi
@Composable
fun ListScreen(
    navigateToTaskScreen: (taskId: Int) -> Unit,
    mainViewModel: MainViewModel
) {
    LaunchedEffect(key1 = true) {
        mainViewModel.getAllTasks()
        mainViewModel.readSortState()
    }
    /* observing states for recomposition */
    val scaffoldState = rememberScaffoldState()
    val allTasks by mainViewModel.allTasks.collectAsState()
    val searchedTask by mainViewModel.searchedTasks.collectAsState()
    val searchAppbarState by mainViewModel.searchAppbarState
    val searchTextState by mainViewModel.searchTextState
    val sortState by mainViewModel.sortState.collectAsState()
    val lowPriorityTasks by mainViewModel.lowPriorityTasks.collectAsState()
    val highPriorityTasks by mainViewModel.highPriorityTasks.collectAsState()
    val action by mainViewModel.action

    DisplaySnackbar(
        scaffoldState = scaffoldState,
        handleDbActions = {
            /* triggering db actions from the viewModel when the action name changes */
            mainViewModel.handleDbActions(actionsDB = action)
        },
        onUndoClicked = {
            /* since the action value is observed above, this will trigger a new recomposition */
            mainViewModel.action.value = it
        },
        taskTitle = mainViewModel.title.value,
        action = action
    )

    Scaffold(
        scaffoldState = scaffoldState,
        content = {
            ListContent(
                allTasks = allTasks,
                searchedTasks = searchedTask,
                lowPriorityTasks = lowPriorityTasks,
                highPriorityTasks = highPriorityTasks,
                sortState = sortState,
                searchAppbarState = searchAppbarState,
                onSwipeToDelete = { actionsDB, todoTask ->
                    mainViewModel.action.value = actionsDB
                    mainViewModel.updateTaskFields(selectedTask = todoTask)
                },
                navigateToTaskScreen = navigateToTaskScreen
            )
        },
        topBar = {
            ListAppBar(
                mainViewModel = mainViewModel,
                searchAppbarState = searchAppbarState,
                searchTextState = searchTextState
            )
        },
        floatingActionButton = {
            ListFab(onFabClicked = navigateToTaskScreen)
        }
    )
}

@Composable
fun ListFab(onFabClicked: (taskId: Int) -> Unit) {
    FloatingActionButton(
        onClick = { onFabClicked(-1) },
        backgroundColor = MaterialTheme.colors.fabBackgroundColor
    ) {
        Icon(
            imageVector = Icons.Filled.Add,
            tint = Color.White,
            contentDescription = stringResource(id = R.string.add_task_fab)
        )
    }
}

@Composable
fun ListAppBar(
    mainViewModel: MainViewModel = hiltViewModel(),
    searchAppbarState: SearchAppbarState,
    searchTextState: String
) {
    when (searchAppbarState) {
        SearchAppbarState.CLOSED -> {
            DefaultAppBar(
                onSearchClicked = {
                    mainViewModel.searchAppbarState.value = SearchAppbarState.OPENED
                },
                onSortClicked = {
                    mainViewModel.persistSortedState(it)
                },
                onDeletedAllConfirmed = {
                    mainViewModel.action.value = ActionsDB.DELETE_ALL
                }
            )
        }
        else -> {
            SearchAppbar(
                text = searchTextState,
                onTextChange = {
                    mainViewModel.searchTextState.value = it
                },
                onClosedClicked = {
                    mainViewModel.searchAppbarState.value = SearchAppbarState.CLOSED
                    mainViewModel.searchTextState.value = ""
                },
                onSearchClicked = {
                    mainViewModel.searchDb(it)
                }
            )
        }
    }
}

@Composable
fun DefaultAppBar(
    onSearchClicked: () -> Unit,
    onSortClicked: (Priority) -> Unit,
    onDeletedAllConfirmed: () -> Unit
) {
    TopAppBar(
        title = {
            Text(
                text = stringResource(id = R.string.app_bar_task_title),
                color = MaterialTheme.colors.topAppbarContentColor
            )
        },
        backgroundColor = MaterialTheme.colors.topAppbarBackground,
        actions = {
            ListAppBarActions(
                onSearchClicked = onSearchClicked,
                onSortClicked = onSortClicked,
                onDeleteAllConfirmed = onDeletedAllConfirmed
            )
        }
    )
}

@Composable
fun ListAppBarActions(
    onSearchClicked: () -> Unit,
    onSortClicked: (Priority) -> Unit,
    onDeleteAllConfirmed: () -> Unit
) {
    var openDialog by remember {
        mutableStateOf(false)
    }

    DisplayAlertDialog(
        title = stringResource(id = R.string.delete_all_tasks),
        message = stringResource(id = R.string.delete_all_task_confirmation),
        openDialog = openDialog,
        closeDialog = { openDialog = false },
        onYesClicked = { onDeleteAllConfirmed() }
    )

    SearchAction(onSearchClicked = onSearchClicked)
    SortAction(onSortClicked = onSortClicked)
    DeleteAllAction(onDeletedAllConfirmed = { openDialog = true })
}

@Composable
fun SearchAction(
    onSearchClicked: () -> Unit = {}
) {
    IconButton(onClick = { onSearchClicked() }) {
        Icon(
            imageVector = Icons.Filled.Search,
            contentDescription = stringResource(R.string.search_icon_description),
            tint = MaterialTheme.colors.topAppbarContentColor
        )
    }
}

@Composable
fun SortAction(
    onSortClicked: (Priority) -> Unit
) {
    var isExpanded by remember {
        mutableStateOf(false)
    }

    IconButton(onClick = { isExpanded = true }) {
        Icon(
            painter = painterResource(id = R.drawable.ic_filter),
            contentDescription = stringResource(
                R.string.filter_action_description
            ),
            tint = MaterialTheme.colors.topAppbarContentColor
        )

        DropdownMenu(
            expanded = isExpanded,
            onDismissRequest = { isExpanded = false }
        ) {
            DropdownMenuItem(onClick = {
                isExpanded = false
                onSortClicked(Priority.LOW)
            }) {
                PriorityItem(priority = Priority.LOW)
            }

            DropdownMenuItem(onClick = {
                isExpanded = false
                onSortClicked(Priority.MEDIUM)
            }) {
                PriorityItem(priority = Priority.MEDIUM)
            }

            DropdownMenuItem(onClick = {
                isExpanded = false
                onSortClicked(Priority.HIGH)
            }) {
                PriorityItem(priority = Priority.HIGH)
            }
        }
    }
}

@Composable
fun DeleteAllAction(
    onDeletedAllConfirmed: () -> Unit = {}
) {
    var isExpanded by remember {
        mutableStateOf(false)
    }

    IconButton(onClick = { isExpanded = true }) {
        Icon(
            imageVector = Icons.Filled.Delete,
            contentDescription = stringResource(
                R.string.delete_all_action_description
            ),
            tint = MaterialTheme.colors.topAppbarContentColor
        )

        DropdownMenu(
            expanded = isExpanded,
            onDismissRequest = { isExpanded = false }
        ) {
            DropdownMenuItem(
                onClick = {
                    onDeletedAllConfirmed()
                    isExpanded = false
                }) {
                Text(
                    modifier = Modifier.padding(start = LARGE_PADDING),
                    text = stringResource(R.string.delete_all_action),
                    style = Typography.subtitle2
                )
            }
        }
    }
}

@Composable
fun SearchAppbar(
    text: String = "",
    onTextChange: (String) -> Unit = {},
    onSearchClicked: (String) -> Unit = {},
    onClosedClicked: () -> Unit = {}
) {
    var trailingIconState by remember {
        mutableStateOf(TrailingIconState.READY_TO_CLOSE)
    }

    Surface(
        modifier = Modifier
            .fillMaxWidth()
            .height(TOP_APPBAR_HEIGHT),
        /* Contains default values used for TopAppBar and BottomAppBar. */
        elevation = AppBarDefaults.TopAppBarElevation,
        color = MaterialTheme.colors.topAppbarBackground
    ) {
        TextField(
            modifier = Modifier.fillMaxWidth(),
            value = text,
            onValueChange = { onTextChange(it) },
            placeholder = {
                Text(
                    modifier = Modifier.alpha(ContentAlpha.medium),
                    text = stringResource(R.string.search_placeholder),
                    color = Color.White
                )
            },
            textStyle = TextStyle(
                color = MaterialTheme.colors.topAppbarContentColor,
                /* extracting only the fontSize */
                fontSize = MaterialTheme.typography.subtitle1.fontSize
            ),
            singleLine = true,
            leadingIcon = {
                IconButton(
                    modifier = Modifier.alpha(ContentAlpha.disabled),
                    onClick = { onSearchClicked(text) }
                ) {
                    Icon(
                        imageVector = Icons.Filled.Search,
                        contentDescription = stringResource(R.string.search_icon_description),
                        tint = MaterialTheme.colors.topAppbarContentColor
                    )
                }
            },
            trailingIcon = {
                IconButton(
                    onClick = {
                        when (trailingIconState) {
                            TrailingIconState.READY_TO_DELETE -> {
                                onTextChange("")
                                trailingIconState = TrailingIconState.READY_TO_CLOSE
                            }
                            TrailingIconState.READY_TO_CLOSE -> {
                                if (text.isNotEmpty()) {
                                    onTextChange("")
                                } else {
                                    onClosedClicked()
                                    trailingIconState = TrailingIconState.READY_TO_DELETE
                                }
                            }
                        }
                    }
                ) {
                    Icon(
                        imageVector = Icons.Filled.Close,
                        contentDescription = stringResource(R.string.close_button_description),
                        tint = MaterialTheme.colors.topAppbarContentColor
                    )
                }
            },
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Search
            ),
            keyboardActions = KeyboardActions(
                onSearch = { onSearchClicked(text) }
            ),
            colors = TextFieldDefaults.textFieldColors(
                cursorColor = MaterialTheme.colors.topAppbarContentColor,
                focusedIndicatorColor = Color.Transparent,
                disabledIndicatorColor = Color.Transparent,
                unfocusedIndicatorColor = Color.Transparent,
                backgroundColor = Color.Transparent
            )
        )
    }
}

@ExperimentalAnimationApi
@ExperimentalMaterialApi
@Composable
fun ListContent(
    allTasks: RequestState<List<TodoTask>>,
    searchedTasks: RequestState<List<TodoTask>>,
    lowPriorityTasks: List<TodoTask>,
    highPriorityTasks: List<TodoTask>,
    sortState: RequestState<Priority>,
    searchAppbarState: SearchAppbarState,
    onSwipeToDelete: (ActionsDB, TodoTask) -> Unit,
    navigateToTaskScreen: (taskId: Int) -> Unit
) {
    if (sortState is RequestState.Success) {
        when {
            searchAppbarState == SearchAppbarState.TRIGGERED -> {
                if (searchedTasks is RequestState.Success) {
                    HandleListContent(
                        tasks = searchedTasks.data,
                        onSwipeToDelete = onSwipeToDelete,
                        navigateToTaskScreen = navigateToTaskScreen
                    )
                }
            }
            sortState.data == Priority.NONE -> {
                if (allTasks is RequestState.Success) {
                    HandleListContent(
                        tasks = allTasks.data,
                        onSwipeToDelete = onSwipeToDelete,
                        navigateToTaskScreen = navigateToTaskScreen
                    )
                }
            }
            sortState.data == Priority.LOW -> {
                HandleListContent(
                    tasks = lowPriorityTasks,
                    onSwipeToDelete = onSwipeToDelete,
                    navigateToTaskScreen = navigateToTaskScreen
                )
            }
            sortState.data == Priority.HIGH -> {
                HandleListContent(
                    tasks = highPriorityTasks,
                    onSwipeToDelete = onSwipeToDelete,
                    navigateToTaskScreen = navigateToTaskScreen
                )
            }
        }
    }
}

@ExperimentalAnimationApi
@ExperimentalMaterialApi
@Composable
fun HandleListContent(
    tasks: List<TodoTask>,
    onSwipeToDelete: (ActionsDB, TodoTask) -> Unit,
    navigateToTaskScreen: (taskId: Int) -> Unit
) {
    if (tasks.isEmpty()) {
        EmptyContent()
    } else {
        DisplayTask(
            tasks = tasks,
            onSwipeToDelete = onSwipeToDelete,
            navigateToTaskScreen = navigateToTaskScreen
        )
    }
}

@ExperimentalMaterialApi
@Composable
fun TaskItem(
    todoTask: TodoTask,
    navigateToTaskScreen: (taskId: Int) -> Unit
) {
    Surface(
        modifier = Modifier.fillMaxWidth(),
        color = MaterialTheme.colors.taskItemBackgroundColor,
        shape = RectangleShape,
        elevation = TASK_ITEM_ELEVATION,
        onClick = {
            navigateToTaskScreen(todoTask.id)
        }
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(all = LARGE_PADDING)
        ) {
            Row {
                Text(
                    modifier = Modifier.weight(8f),
                    text = todoTask.title,
                    color = MaterialTheme.colors.taskItemTextColor,
                    style = MaterialTheme.typography.h5,
                    fontWeight = FontWeight.Bold,
                    maxLines = 1
                )
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .weight(1f),
                    contentAlignment = Alignment.TopEnd
                ) {
                    Canvas(modifier = Modifier.size(PRIORITY_INDICATOR_SIZE)) {
                        drawCircle(color = todoTask.priority.color)
                    }
                }
            }
            Text(
                modifier = Modifier.fillMaxWidth(),
                text = todoTask.description,
                color = MaterialTheme.colors.taskItemTextColor,
                style = MaterialTheme.typography.subtitle1,
                maxLines = 2,
                overflow = TextOverflow.Ellipsis
            )
        }
    }
}

@ExperimentalAnimationApi
@ExperimentalMaterialApi
@Composable
fun DisplayTask(
    tasks: List<TodoTask>,
    onSwipeToDelete: (ActionsDB, TodoTask) -> Unit,
    navigateToTaskScreen: (taskId: Int) -> Unit
) {
    LazyColumn {
        items(
            items = tasks,
            key = { it.id }
        ) {
            val dismissState = rememberDismissState()
            val dismissDirection = dismissState.dismissDirection
            val isDismissed = dismissState.isDismissed(DismissDirection.EndToStart)
            val animatedDegrees by animateFloatAsState(
                targetValue = if (dismissState.targetValue == DismissValue.Default) 0f else -45f
            )
            var itemAppeared by remember {
                mutableStateOf(false)
            }

            if (isDismissed && dismissDirection == DismissDirection.EndToStart) {
                val scope = rememberCoroutineScope()
                /* delay for the animation */
                scope.launch {
                    delay(300)
                    onSwipeToDelete(ActionsDB.DELETE, it)
                }
            }

            LaunchedEffect(key1 = true) {
                itemAppeared = true
            }

            AnimatedVisibility(
                visible = itemAppeared && !isDismissed,
                enter = expandVertically(
                    animationSpec = tween(durationMillis = 300)
                ),
                exit = shrinkVertically(
                    animationSpec = tween(durationMillis = 300)
                )
            ) {
                SwipeToDismiss(
                    state = dismissState,
                    directions = setOf(DismissDirection.EndToStart),
                    dismissThresholds = { FractionalThreshold(fraction = 0.2f) },
                    background = { RedBackground(degrees = animatedDegrees) },
                    dismissContent = {
                        TaskItem(
                            todoTask = it,
                            navigateToTaskScreen = navigateToTaskScreen
                        )
                    }
                )
            }
        }
    }
}

@Composable
fun DisplaySnackbar(
    scaffoldState: ScaffoldState,
    handleDbActions: () -> Unit,
    onUndoClicked: (ActionsDB) -> Unit,
    taskTitle: String,
    action: ActionsDB
) {
    handleDbActions()
    val scope = rememberCoroutineScope()

    LaunchedEffect(key1 = action) {
        if (action != ActionsDB.NO_ACTION) {
            scope.launch {
                val snackBarResult = scaffoldState.snackbarHostState
                    .showSnackbar(
                        message = setMessage(action = action, taskTitle = taskTitle),
                        actionLabel = if (action.name == DELETE_LABEL) UNDO_LABEL else CONFIRM_LABEL,
                        duration = SnackbarDuration.Short
                    )
                undoDeletedTask(
                    action = action,
                    snackbarResult = snackBarResult,
                    onUndoClicked = onUndoClicked
                )
            }
        }
    }
}

private fun setMessage(action: ActionsDB, taskTitle: String): String {
    return when (action) {
        ActionsDB.DELETE_ALL -> DELETED_ALL_TASKS_MESSAGE
        else -> "${action.name} : $taskTitle"
    }
}

/*private fun setActionLabel(action: ActionsDB) =
    if (action.name == DELETE_LABEL) UNDO_LABEL else CONFIRM_LABEL*/

private fun undoDeletedTask(
    action: ActionsDB,
    snackbarResult: SnackbarResult,
    onUndoClicked: (ActionsDB) -> Unit
) {
    /* checking if the snackbar action label was clicked and if the action was DELETE */
    if (snackbarResult == SnackbarResult.ActionPerformed && action == ActionsDB.DELETE) {
        onUndoClicked(ActionsDB.UNDO)
    }
}

@Composable
fun RedBackground(degrees: Float) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(HighPriorityColor)
            .padding(horizontal = LARGEST_PADDING),
        contentAlignment = Alignment.CenterEnd
    ) {
        Icon(
            modifier = Modifier.rotate(degrees = degrees),
            imageVector = Icons.Filled.Delete,
            contentDescription = stringResource(id = R.string.delete_button_description),
            tint = Color.White
        )
    }
}

/*
@ExperimentalMaterialApi
@Preview
@Composable
fun TaskItemPreview() {
    Surface(color = MaterialTheme.colors.taskItemBackgroundColor){
        TaskItem(
            todoTask = TodoTask(
                1, "title preview",
                "description preview",
                Priority.LOW
            ),
            navigateToTaskScreen = {}
        )
    }
}*/
