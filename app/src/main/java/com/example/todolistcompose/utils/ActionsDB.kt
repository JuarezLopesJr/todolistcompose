package com.example.todolistcompose.utils

enum class ActionsDB {
    ADD,
    UPDATE,
    DELETE,
    DELETE_ALL,
    UNDO,
    NO_ACTION
}

fun String?.toAction(): ActionsDB {
    return when {
        this == "ADD" -> {
            ActionsDB.ADD
        }
        this == "UPDATE" -> {
            ActionsDB.UPDATE
        }
        this == "DELETE" -> {
            ActionsDB.DELETE
        }
        this == "DELETE_ALL" -> {
            ActionsDB.DELETE_ALL
        }
        this == "UNDO" -> {
            ActionsDB.UNDO
        }
        else -> {
            ActionsDB.NO_ACTION
        }
    }
}