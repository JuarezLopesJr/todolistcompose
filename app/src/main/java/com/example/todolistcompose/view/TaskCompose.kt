package com.example.todolistcompose.view

import android.content.Context
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.activity.OnBackPressedDispatcher
import androidx.activity.compose.LocalOnBackPressedDispatcherOwner
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Delete
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import com.example.todolistcompose.R
import com.example.todolistcompose.model.Priority
import com.example.todolistcompose.model.TodoTask
import com.example.todolistcompose.ui.theme.LARGE_PADDING
import com.example.todolistcompose.ui.theme.MEDIUM_PADDING
import com.example.todolistcompose.ui.theme.topAppbarBackground
import com.example.todolistcompose.ui.theme.topAppbarContentColor
import com.example.todolistcompose.utils.ActionsDB
import com.example.todolistcompose.viewmodel.MainViewModel

@Composable
fun TaskScreen(
    mainViewModel: MainViewModel,
    selectedTask: TodoTask?,
    navigateToListScreen: (ActionsDB) -> Unit
) {
    val title by mainViewModel.title
    val description by mainViewModel.description
    val priority by mainViewModel.priority
    /* getting Compose context */
    val context = LocalContext.current

    BackButtonHandler(onBackPressed = { navigateToListScreen(ActionsDB.NO_ACTION) })

    Scaffold(
        content = {
            TaskContent(
                title = title,
                onTitleChange = {
                    mainViewModel.updateTitle(it)
                },
                description = description,
                onDescriptionChange = {
                    mainViewModel.description.value = it
                },
                priority = priority,
                onPrioritySelected = {
                    mainViewModel.priority.value = it
                }
            )
        },
        topBar = {
            TaskAppBar(
                selectedTask = selectedTask,
                navigateToListScreen = {
                    when {
                        it == ActionsDB.NO_ACTION -> {
                            navigateToListScreen(it)
                        }
                        mainViewModel.validateFields() -> {
                            navigateToListScreen(it)
                        }
                        else -> {
                            displayToast(context = context)
                        }
                    }
                }
            )
        }
    )
}

fun displayToast(context: Context) {
    Toast.makeText(
        context,
        context.getString(R.string.validation_fields_warning),
        Toast.LENGTH_SHORT
    ).show()
}

@Composable
fun TaskAppBar(selectedTask: TodoTask?, navigateToListScreen: (ActionsDB) -> Unit) {
    if (selectedTask == null) {
        NewTaskBar(navigateToListScreen = navigateToListScreen)
    } else {
        ExistingTaskBar(
            selectedTask = selectedTask,
            navigateToListScreen = navigateToListScreen
        )
    }
}

@Composable
fun NewTaskBar(navigateToListScreen: (ActionsDB) -> Unit) {
    TopAppBar(
        navigationIcon = {
            BackAction(onBackClicked = navigateToListScreen)
        },
        title = {
            Text(
                text = stringResource(R.string.add_task),
                color = MaterialTheme.colors.topAppbarContentColor
            )
        },
        backgroundColor = MaterialTheme.colors.topAppbarBackground,
        actions = {
            AddAction(onAddClicked = navigateToListScreen)
        }
    )
}

@Composable
fun BackAction(
    onBackClicked: (ActionsDB) -> Unit
) {
    IconButton(onClick = { onBackClicked(ActionsDB.NO_ACTION) }) {
        Icon(
            imageVector = Icons.Filled.ArrowBack,
            contentDescription = stringResource(R.string.action_back_description),
            tint = MaterialTheme.colors.topAppbarContentColor
        )
    }
}

@Composable
fun AddAction(
    onAddClicked: (ActionsDB) -> Unit
) {
    IconButton(onClick = { onAddClicked(ActionsDB.ADD) }) {
        Icon(
            imageVector = Icons.Filled.Check,
            contentDescription = stringResource(R.string.add_task_button_description),
            tint = MaterialTheme.colors.topAppbarContentColor
        )
    }
}

@Composable
fun ExistingTaskBar(
    selectedTask: TodoTask,
    navigateToListScreen: (ActionsDB) -> Unit
) {
    TopAppBar(
        navigationIcon = {
            CloseAction(onCloseClicked = navigateToListScreen)
        },
        title = {
            Text(
                text = selectedTask.title,
                color = MaterialTheme.colors.topAppbarContentColor,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis
            )
        },
        backgroundColor = MaterialTheme.colors.topAppbarBackground,
        actions = {
            ExistingAppbarActions(
                selectedTask = selectedTask,
                navigateToListScreen = navigateToListScreen
            )
        }
    )
}

@Composable
fun CloseAction(
    onCloseClicked: (ActionsDB) -> Unit
) {
    IconButton(onClick = { onCloseClicked(ActionsDB.NO_ACTION) }) {
        Icon(
            imageVector = Icons.Filled.Close,
            contentDescription = stringResource(R.string.close_button_description),
            tint = MaterialTheme.colors.topAppbarContentColor
        )
    }
}

@Composable
fun DeleteAction(
    onDeleteClicked: () -> Unit
) {
    IconButton(onClick = { onDeleteClicked() }) {
        Icon(
            imageVector = Icons.Filled.Delete,
            contentDescription = stringResource(R.string.delete_button_description),
            tint = MaterialTheme.colors.topAppbarContentColor
        )
    }
}

@Composable
fun UpdateAction(
    onUpdateClicked: (ActionsDB) -> Unit
) {
    IconButton(onClick = { onUpdateClicked(ActionsDB.UPDATE) }) {
        Icon(
            imageVector = Icons.Filled.Check,
            contentDescription = stringResource(R.string.update_button_description),
            tint = MaterialTheme.colors.topAppbarContentColor
        )
    }
}

@Composable
fun TaskContent(
    title: String,
    onTitleChange: (String) -> Unit,
    description: String,
    onDescriptionChange: (String) -> Unit,
    priority: Priority,
    onPrioritySelected: (Priority) -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(all = LARGE_PADDING)
    ) {
        OutlinedTextField(
            modifier = Modifier.fillMaxWidth(),
            value = title,
            onValueChange = { onTitleChange(it) },
            label = { Text(text = stringResource(R.string.title_label)) },
            textStyle = MaterialTheme.typography.body1,
            singleLine = true
        )
        Divider(
            modifier = Modifier.height(MEDIUM_PADDING),
            color = MaterialTheme.colors.background
        )

        PriorityDropDown(
            priority = priority,
            onPrioritySelected = onPrioritySelected
        )
        OutlinedTextField(
            modifier = Modifier.fillMaxSize(),
            value = description,
            onValueChange = { onDescriptionChange(it) },
            label = { Text(text = stringResource(R.string.description_label)) },
            textStyle = MaterialTheme.typography.body1
        )
    }
}

@Composable
fun ExistingAppbarActions(selectedTask: TodoTask, navigateToListScreen: (ActionsDB) -> Unit) {
    var openDialog by remember {
        mutableStateOf(false)
    }

    DisplayAlertDialog(
        title = stringResource(
            id = R.string.delete_task,
            selectedTask.title
        ),
        message = stringResource(
            id = R.string.delete_task_confirmation,
            selectedTask.title
        ),
        openDialog = openDialog,
        closeDialog = { openDialog = false },
        onYesClicked = { navigateToListScreen(ActionsDB.DELETE) })

    DeleteAction(onDeleteClicked = { openDialog = true })
    UpdateAction(onUpdateClicked = navigateToListScreen)
}

@Composable
fun BackButtonHandler(
    backDispatcher: OnBackPressedDispatcher? =
        LocalOnBackPressedDispatcherOwner.current?.onBackPressedDispatcher,
    onBackPressed: () -> Unit
) {
    val currentOnBackPressed by rememberUpdatedState(newValue = onBackPressed)

    val backCallback = remember {
        object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                currentOnBackPressed()
            }
        }
    }

    DisposableEffect(key1 = backDispatcher) {
        backDispatcher?.addCallback(backCallback)

        onDispose {
            backCallback.remove()
        }
    }
}