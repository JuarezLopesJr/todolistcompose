package com.example.todolistcompose.model

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import androidx.room.Update
import kotlinx.coroutines.flow.Flow

@Dao
interface TodoDao {

    @Query("SELECT * FROM todo_table WHERE title LIKE :searchQuery OR description LIKE :searchQuery")
    fun searchDatabaseDao(searchQuery: String): Flow<List<TodoTask>>

    @Query("SELECT * FROM todo_table ORDER BY id ASC")
    fun getAllTasksDao(): Flow<List<TodoTask>>

    @Query("SELECT * FROM todo_table WHERE id = :taskId")
    fun getSelectedTaskDao(taskId: Int): Flow<TodoTask>

    @Insert(onConflict = REPLACE)
    suspend fun addTaskDao(task: TodoTask)

    @Update
    suspend fun updateTaskDao(task: TodoTask)

    @Delete
    suspend fun deleteTaskDao(task: TodoTask)

    @Query("DELETE FROM todo_table")
    suspend fun deleteAllTasksDao()

    /* 'L%' means, start with this letter, it's case sensitive.
    The priority will be saved as string (HIGH, LOW, MEDIUM) */
    @Query("SELECT * FROM todo_table ORDER BY CASE WHEN priority LIKE 'L%' THEN 1 WHEN priority LIKE 'M%' THEN 2 WHEN priority LIKE 'H%' THEN 3 END")
    fun sortByLowPriorityDao(): Flow<List<TodoTask>>

    @Query("SELECT * FROM todo_table ORDER BY CASE WHEN priority LIKE 'M%' THEN 1 WHEN priority LIKE 'H%' THEN 2 WHEN priority LIKE 'L%' THEN 3 END")
    fun sortByMediumPriorityDao(): Flow<List<TodoTask>>

    @Query("SELECT * FROM todo_table ORDER BY CASE WHEN priority LIKE 'H%' THEN 1 WHEN priority LIKE 'M%' THEN 2 WHEN priority LIKE 'L%' THEN 3 END")
    fun sortByHighPriorityDao(): Flow<List<TodoTask>>
}